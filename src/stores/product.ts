import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type Product from '@/types/Product'
import productService from '@/services/product'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'

export const useProductStore = defineStore('Product', () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false)
  const products = ref<Product[]>([])
  const editedProduct = ref<Product>({ name: '', price: 0 })

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog)
    if (!newDialog) {
      editedProduct.value = { name: '', price: 0 }
    }
  })

  async function getProduct() {
    try {
      loadingStore.isLoading = true;
      const res = await productService.getProducts()
      products.value = res.data
      // console.log(res);
    } catch (e) {
      console.log(e)
      messageStore.showError("ไม่สามารถดึงข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveProduct() {
    loadingStore.isLoading = true;
    try {
      if (editedProduct.value.id) {
        const res = await productService.updateProduct(editedProduct.value.id, editedProduct.value)
      } else {
        const res = await productService.saveProduct(editedProduct.value)
      }

      await getProduct()
      dialog.value = false
    } catch (e) {
      console.log(e)
      messageStore.showError("ไม่สามารถบันทึก Product ได้");
    }
    loadingStore.isLoading = false;
  }

  // function clearProduct() {
  //   editedProduct.value = {name: "", price: 0};
  // }

  function editProduct(product: Product) {
    console.log(product)
    editedProduct.value = JSON.parse(JSON.stringify(product));
    dialog.value = true
  }

  async function deleteProduct(id:number) {
    loadingStore.isLoading = true;
    try {
      const res = await productService.deleteProduct(id);
      await getProduct();
    } catch (e) {
      console.log(e)
      messageStore.showError("ไม่สามารถลบ Product ได้");
    }
    loadingStore.isLoading = false;
  }

  return { products, getProduct, dialog, editedProduct, saveProduct, editProduct, deleteProduct }
})
